/**
 * Copyright@xiaocong.tv 2012
 */
package cn.uncode.baas.server.internal.module.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * @author caijun.du
 * @version
 * @date 2012-8-27
 */
public class MailAuthenticator extends Authenticator {
	private String username = null;
	private String password = null;

	public MailAuthenticator() {
	};

	public MailAuthenticator(String username, String password) {
		this.username = username;
		this.password = password;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}

}
